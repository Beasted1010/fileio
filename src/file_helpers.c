
#include "file_helpers.h"
#include "string.h"
#include "stdlib.h"

ReturnCode GetNumLines(FILE* fp, int* out)
{
    if(!fp)
    {
        printf("WARNING: The file has not yet been created!\n");
        *out = 0;
        return WARNING_0;
    }

    // We wish to remain unobstructive and preserve the callers current file position
    fpos_t pos_save;
    fgetpos(fp, &pos_save);

    // Start at the beginning of the file
    fseek(fp, 0, SEEK_SET);

    char ch = 0; // Initialize to some non-negative value (to avoid EOF clash)
    while(ch != EOF)
    {
        ch = getc(fp);

        if(ch == EOF)
        {
            break;
        }

        // If we reached the end of a line, count it as a new line
        if(ch == '\n')
        {
            (*out)++;
        }
    }

    fsetpos(fp, &pos_save);

    return SUCCESS;
}

ReturnCode GetFileSize(FILE* fp, long int* out)
{
    if(!fp)
    {
        printf("WARNING: The file has not yet been created!\n");
        *out = 0;
        return WARNING_0;
    }

    // We wish to remain unobstructive and preserve the callers current file position
    fpos_t pos_save;
    fgetpos(fp, &pos_save);

    // ftell returns the current position of the file pointer, so go to end to capture full file size
    fseek(fp, 0, SEEK_END);

    // Grab the amount of bytes up to the current point
    // ftell returns the current position of the file pointer (ASSUMING 1 byte = 1 char), so will be the current byte/char
    *out = ftell(fp);

    fsetpos(fp, &pos_save);

    return SUCCESS;
}

ReturnCode GetCurrentBytePosition(FILE* fp, long int* out)
{
    if(!fp)
    {
        printf("WARNING: The file has not yet been created!\n");
        *out = 0;
        return WARNING_0;
    }

    // Grab the amount of bytes up to the current point
    // ftell returns the current position of the file pointer (ASSUMING 1 byte = 1 char), so will be the current byte/char
    *out = ftell(fp);

    return SUCCESS;
}

// TODO: Check accuracy of directoryPathLength (and absolute filepath length)
ReturnCode FindAbsoluteDirectoryPath(char* filePath, int pathLength, char** directoryPath, int* directoryPathLength)
{
    char* filename = malloc(sizeof(char));
    if(!filename)
    {
        printf("ERROR: Failed to allocate memory for variable!\n");
        return ERROR_0;
    }

    int filename_length = 0;

    TrimPathForFileName(filePath, pathLength, &filename, &filename_length);

    // +1 to the filename_length because the current length does not include null character
    *directoryPathLength = pathLength - (filename_length + 1);

    int length = ++(*directoryPathLength);

    (*directoryPath) = realloc(*directoryPath, sizeof(char) * length);

    for(int i = 0; i < length; i++)
    {
        (*directoryPath)[i] = filePath[i];
    }

    (*directoryPath)[length] = '\0';

    return SUCCESS;

}

ReturnCode TrimPathForFileName(char* filePath, int pathLength, char** trimmedPath, int* trimmedPathLength)
{
    char* result = malloc(sizeof(char) * pathLength);

    if(!result)
    {
        printf("ERROR: Failed to allocate memory for the result!\n");
        return ERROR_0;
    }

    int size_of_file_name = 0;

    // Start at the tail, go until we hit a slash (sign that we hit a directory)
    int i = pathLength;
    for(i; i > 0; i--)
    {
        if(filePath[i] == '\\' || filePath[i] == '/')
        {
            break;
        }
        size_of_file_name++;
        result[pathLength - i] = filePath[i];
    }

    if(i == 0)
    {
        printf("ERROR: The file name was not found in the path!\n");
        printf("Perhaps this is because the path length is incorrect?\n");
        return ERROR_1;
    }

    result[size_of_file_name] = '\0';

    *trimmedPathLength = --size_of_file_name;

    (*trimmedPath) = realloc((*trimmedPath), sizeof(char) * size_of_file_name);

    if(!(*trimmedPath))
    {
        printf("ERROR: Failed to reallocate memory for the trimmed path!\n");
        return ERROR_0;
    }

    // Increment over the entire string and set the values of result to trimmed path
    // We read in the string backwards, so compensate by storing values in reverse order
    for(int i = size_of_file_name; i > 0; i--)
    {
        // size_of_file_name is the size of the entire array, but 0 indexed, so size of string - 1 is lasat element
        (*trimmedPath)[(size_of_file_name) - i] = result[i];
    }

    // Add the null terminating character onto the tail of the string
    (*trimmedPath)[size_of_file_name] = '\0';

    free(result);

    return SUCCESS;
}

ReturnCode PrintFileInfo(FileInfo* fileInfo)
{
    printf("\n\n");
    printf("File metadata:\n");
    printf("---------------------------\n");

    printf("Filename: %s\n", fileInfo->filename);
    printf("Filename length: %i\n", fileInfo->filename_length);

    printf("File size: %ld\n", fileInfo->file_size);
    printf("Number of lines: %i\n", fileInfo->num_lines);
    printf("File permissions: %s\n", fileInfo->file_permissions);
    printf("Current byte position in file: %ld\n", fileInfo->curr_position);

    printf("Absolute FILE path: %s\n", fileInfo->absolute_file_path);
    printf("Absolute FILE path length: %i\n", fileInfo->absolute_file_path_length);

    printf("Absolute DIRECTORY path: %s\n", fileInfo->absolute_directory_path);
    printf("Absolute DIRECTORY path length: %i\n", fileInfo->absolute_directory_path_length);

    printf("\n\n");

    return SUCCESS;
}

ReturnCode FillFileInfo(FileInfo* fileInfo, FILE* asdf, const char* relativePath, const char* filePermissions)
{
    fileInfo->file_permissions = malloc(sizeof(char) * MAX_PERM_LENGTH);

    if(!fileInfo->file_permissions)
    {
        printf("ERROR: Failed to allocate memory for file permissions!\n");
        return ERROR_0;
    }

    // Temporary length of the file path
    fileInfo->absolute_file_path_length = 150;
    fileInfo->absolute_file_path = malloc(sizeof(char) * fileInfo->absolute_file_path_length);

    if(!fileInfo->absolute_file_path)
    {
        printf("ERROR: Failed to allocate memory for absolute file path!\n");
        return ERROR_0;
    }

    strcpy(fileInfo->file_permissions, filePermissions);

    if( !(_fullpath( fileInfo->absolute_file_path, relativePath, fileInfo->absolute_file_path_length) ) )
    {
        printf("ERROR: Invalid relative path of %s\n", relativePath);
        return ERROR_0;
    }

    // Resize file path length to a more precise value
    fileInfo->absolute_file_path_length = strlen(fileInfo->absolute_file_path);
    fileInfo->absolute_file_path = realloc(fileInfo->absolute_file_path,
                                            sizeof(char) * fileInfo->absolute_file_path_length);

    if(!fileInfo->absolute_file_path)
    {
        printf("ERROR: Failed to reallocate memory for absolute file path!\n");
        return ERROR_0;
    }

    fileInfo->filename = malloc(sizeof(char) * fileInfo->absolute_file_path_length);
    fileInfo->absolute_directory_path = malloc(sizeof(char) * fileInfo->absolute_file_path_length);

    if(!fileInfo->filename)
    {
        printf("ERROR: Failed to allocate memory for filename!\n");
        return ERROR_0;
    }

    if(!fileInfo->absolute_directory_path)
    {
        printf("ERROR: Failed to allocate memory for absolute directory path!\n");
        return ERROR_0;
    }

    TrimPathForFileName(fileInfo->absolute_file_path, fileInfo->absolute_file_path_length,
                        &fileInfo->filename, &fileInfo->filename_length);

    FindAbsoluteDirectoryPath(fileInfo->absolute_file_path, fileInfo->absolute_file_path_length,
                              &fileInfo->absolute_directory_path, &fileInfo->absolute_directory_path_length);

    printf("Opening file: %s\n", fileInfo->absolute_file_path);
    FILE* fp = fopen(fileInfo->absolute_file_path, fileInfo->file_permissions);

    if(!fp)
    {
      printf("File %s was unable to be opened with permissions: %s\n", fileInfo->filename, fileInfo->file_permissions);
    }

    fileInfo->file_pointer = fp;

    int num_lines;
    long int file_size;
    long int curr_pos;

    if(GetNumLines(fileInfo->file_pointer, &num_lines))
    {
        return ERROR_1;
    }
    fileInfo->num_lines = num_lines;

    if(GetFileSize(fileInfo->file_pointer, &file_size))
    {
        return ERROR_2;
    }
    fileInfo->file_size = file_size;

    if(GetCurrentBytePosition(fileInfo->file_pointer, &curr_pos))
    {
        return ERROR_3;
    }
    fileInfo->curr_position = curr_pos;

    PrintFileInfo(fileInfo);

    return SUCCESS;
}

ReturnCode DestroyFileInfo(FileInfo* fileInfo)
{
    printf("Closing file: %s\n", fileInfo->absolute_file_path);
    if(fclose(fileInfo->file_pointer))
    {
        printf("ERROR: File %s failed to close successfully!\n", fileInfo->absolute_file_path);
        return ERROR_0;
    }

    free(fileInfo->filename);
    free(fileInfo->file_permissions);
    free(fileInfo->absolute_file_path);
    free(fileInfo->absolute_directory_path);

    free(fileInfo);

    return SUCCESS;
}


ReturnCode EmptyFile(FileInfo* fileInfo)
{
    if(!fileInfo->file_pointer)
    {
        printf("ERROR: The file has not yet been created!\n");
        return ERROR_0;
    }

    freopen(fileInfo->filename, "w", fileInfo->file_pointer);

    return SUCCESS;
}
