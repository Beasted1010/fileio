
#ifndef COMMON_H
#define COMMON_H

#include "stdio.h"

#define MAX_PERM_LENGTH 5

// TODO: Have a standard structure for all error codes? -> Such as ERROR_0 is inability to allocate memory, _3 is misc?
typedef enum ReturnCode {
    ERROR_0 = -4,
    ERROR_1,
    ERROR_2,
    ERROR_3,
    SUCCESS = 0,
    WARNING_0,
    WARNING_1,
    WARNING_2,
    WARNING_3
} ReturnCode;

/*
    num_lines -> The number of lines the file contains
    curr_position -> The current position of the file pointer (each character is a byte) -> Max: 2,147,483,647
    file_size -> The size of the file (in bytes) -> Max: 2,147,483,647
*/
typedef struct FileInfo {
    FILE* file_pointer;
    char* absolute_file_path;
    int absolute_file_path_length;
    char* absolute_directory_path;
    int absolute_directory_path_length;
    char* filename;
    int filename_length;
    char* file_permissions;
    int num_lines;
    long int file_size;
    long int curr_position;
} FileInfo;

/* Retrieve the number of lines for the given file
    WARNING_0 -> File has not yet been created, out = 0
*/
ReturnCode GetNumLines(FILE* fp, int* out);

/* Retrieve the size of the given file
    WARNING_0 -> File has not yet been created, out = 0
*/
ReturnCode GetFileSize(FILE* fp, long int* out);

/* Retrieve the size of the given file
    WARNING_0 -> File has not yet been created, out = 0
*/
ReturnCode GetCurrentBytePosition(FILE* fp, long int* out);

/* Retrieve the size of the given file
    ERROR_0 -> Failed to grab the absolute file path from the given relative path
    ERROR_1 -> Retrieving the number of lines for the file failed
    ERROR_2 -> Retrieving the file size failed
    ERROR_3 -> Retrieving the current position of the file failed
*/
ReturnCode FillFileInfo(FileInfo* fileInfo, FILE* fp, const char* relativePath, const char* filePermissions);

/* Retrieve the size of the given file
    ERROR_0 -> Failed to close the file pointed to by file pointer
*/
ReturnCode DestroyFileInfo(FileInfo* fileInfo);

// TODO: Commment
ReturnCode EmptyFile(FileInfo* fileInfo);
ReturnCode PrintFileInfo(FileInfo* fileInfo);
ReturnCode TrimPathForFileName(char* filePath, int pathLength, char** trimmedPath, int* trimmedPathLength);
ReturnCode FindAbsoluteDirectoryPath(char* filePath, int pathLength, char** directoryPath, int* directoryPathLength);

#endif
