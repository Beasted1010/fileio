

#ifndef OUTPUT_FILE_H
#define OUTPUT_FILE_H

#include "stdio.h"

#include "file_helpers.h"

// TODO: I am thinking output_file.h should be for functionality that includes creating a new file...?

static char input_path[] = "../files/";
static char output_path[] = "../files/";

/* Delete the lines the user specified for the given input fp, put the results in the output fp
    ERROR_0 -> File has not been created
    ERROR_1 -> Couldn't point the file pointer to the beginning of the file
    ERROR_2 -> Not all requested lines were deleted
    WARNING_0 -> Extra lines were deleted, these lines will be displayed in stdout
*/
ReturnCode DeleteLines(FileInfo* fileInfo, int* lines, int num_lines);

/* Delete the rest of the file starting from the current position in the infile and print result to outfile
    ERROR_0 -> File has not been created
*/
ReturnCode DeleteRestOfFile(FileInfo* fileInfo);

ReturnCode DeleteRestOfLine(FILE* in_fp, FILE* out_fp);

ReturnCode GetFilenamePrefix(char* filename, char** prefix);
ReturnCode GetFilenameSuffix(char* filename, char** suffix);
ReturnCode CreateReplicaFile(FileInfo* fileInfo, char* new_file_name, char** fileReplica);




#endif
