
@echo off

cd obj

gcc -c -I ../inc ../src/output_file.c ../src/file_helpers.c ../src/test_environment.c

cd ../lib

ar rcs libfile_io.a ../obj/output_file.o ../obj/file_helpers.o

cd ../bin

gcc -o run_tests.exe -L ../lib ../obj/test_environment.o -lfile_io

cd ..
